/*
 * FUSE: Filesystem in Userspace
 * DSO 2020
 * Prueba proyecto librería de música
*/

#define FUSE_USE_VERSION 26



#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>
#include <id3v2lib.h>
#include <sys/types.h>
#include <dirent.h>
#include <libgen.h>

#include "structures.h"


// Si está ya en la lista, devuelvo 1, si no un 0
static int esta_artista(Artista art, char* tag_artista) {
    Artista aux = art;

    while(aux) {
        //fprintf(stderr,"##%s##\n##%s##\n",aux->tag_artista,tag_artista);
        if(strcmp(aux->tag_artista,tag_artista)==0)	return 1;
        aux=aux->sig;
    }
    return 0;
}

static int lista_size(Artista art) {
    Artista aux = art;
    int contador=0;
    while(aux) {
        contador++;
        aux=aux->sig;
    }
    return contador;
}

void limpia_etiqueta(char * tag)
{
    int len= strlen(tag);
    for(int i= 0; i<len; i++)
    {
        if (tag[i]=='/' || tag[i]==' ') tag[i]='_';
    }

}

// Inserto cierto artista
void insertar_artista(Artista *lista_art, char* tag_artista) {
    Artista nuevoNodo, ptr;

    if(esta_artista(*lista_art, tag_artista)==0) {
        //	fprintf(stderr,"   NUEVO ARTISTA\n");
        nuevoNodo=malloc(sizeof(struct lista_artista));
        nuevoNodo->tag_artista=strdup(tag_artista);
        nuevoNodo->sig=NULL;
        if(*lista_art == NULL) {		// lista vacia

            *lista_art=nuevoNodo;
        } else {						// insertamos al final
            ptr=*lista_art;
            while(ptr->sig!=NULL) ptr=ptr->sig;

            ptr->sig=nuevoNodo;
        }
    }
}


// Crea una lista de artistas
Artista rellenar_lista_artistas(const char * path) {
    Artista lista=NULL;
    char lpath[512];
    ID3v2_tag* artist_tag;
    ID3v2_frame* artist_frame;
    ID3v2_frame_text_content* artist_content;
    DIR *dir;
    struct dirent *dp;
    char * file_name;
    dir = opendir(path);
    while ((dp=readdir(dir)) != NULL)
    {
        strcpy(lpath,path);
        strcat(lpath,"/");
        strcat(lpath,dp->d_name);
        fprintf(stderr,"  fichero: %s\n",lpath);

        artist_tag = load_tag(lpath);
        if(artist_tag==NULL) continue;
        artist_frame = tag_get_artist(artist_tag);

        artist_content = parse_text_frame_content(artist_frame);

        //    fprintf(stderr,"  Artist TAG: %s\n",artist_content->data);
        limpia_etiqueta(artist_content->data);
        insertar_artista(&lista, artist_content->data);
        //	fprintf(stderr,"  -> insertado OK\n");

    }
    return lista;
}




int existe_fichero(const char * nombre, const char *path)
{
    DIR *dir;
    struct dirent *dp;
    char * file_name;
    dir = opendir(path);
    while ((dp=readdir(dir)) != NULL)
        if ( !strcmp(dp->d_name, nombre))  {
            closedir(dir);
            return 1;
        }
    closedir(dir);
    return 0;

}




/*
 *  Para usar los datos pasados a FUSE usar en las funciones:
 *
	struct structura_mis_datos *mis_datos= (struct structura_mis_datos *) fuse_get_context()->private_data;
 *
 * */



int mp3_getattr(const char *path, struct stat *stbuf) {

    int res = 0;
    struct estructura_mis_datos* mis_datos = (struct estructura_mis_datos *)fuse_get_context()->private_data;

    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 4; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)
        stbuf->st_uid = mis_datos->st_uid; //propietario del fichero
        stbuf->st_gid = mis_datos->st_gid; // grupo propietario del fichero

        stbuf->st_atime = mis_datos->st_atime; //horas de modificación etc ...
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096; //tamaño
        stbuf->st_blocks = 2; // tamaño divido entre 512

    } else if (strcmp(path, "/Artistas") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2 + lista_size(mis_datos->lista_art);
        stbuf->st_uid = mis_datos->st_uid;
        stbuf->st_gid = mis_datos->st_gid;

        stbuf->st_atime = mis_datos->st_atime;
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096;
        stbuf->st_blocks = 2;

    } else if (strcmp(path, "/Favoritos") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
        stbuf->st_uid = mis_datos->st_uid;
        stbuf->st_gid = mis_datos->st_gid;

        stbuf->st_atime = mis_datos->st_atime;
        stbuf->st_mtime = mis_datos->st_mtime;
        stbuf->st_ctime = mis_datos->st_ctime;
        stbuf->st_size = 4096;
        stbuf->st_blocks = 2;

    } else if(strncmp(path, "/Artistas/",10) == 0) {

        char *token = strtok((char*)path, "/"); // busca primera parte del path
        token = strtok(NULL,"/");
        if(esta_artista(mis_datos->lista_art,token)) // es un directorio de artista
        {
            token = strtok(NULL,"/");
            if(token==NULL)
            {   // es el directorio
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
                stbuf->st_uid = mis_datos->st_uid;
                stbuf->st_gid = mis_datos->st_gid;

                stbuf->st_atime = mis_datos->st_atime;
                stbuf->st_mtime = mis_datos->st_mtime;
                stbuf->st_ctime = mis_datos->st_ctime;
                stbuf->st_size = 4096;
                stbuf->st_blocks = 2;
            }
            else // es un fichero dentro del directorio
            {
                if(existe_fichero(token, mis_datos->path))
                {
                    struct stat  fileStat;
                    char lpath[1024];
                    strcpy(lpath,mis_datos->path);
                    strcat(lpath,"/");
                    strcat(lpath,token);
                    stat(lpath, &fileStat);  //leemos datos del fichero original

                    stbuf->st_mode = S_IFREG | 0444;
                    stbuf->st_nlink = 1; //numero de entradas en sistema de ficheros (directorios por defecto 2 -> . y el ..)

                    stbuf->st_uid = fileStat.st_uid; //propietario del fichero
                    stbuf->st_gid = fileStat.st_gid; // grupo propietario del fichero
                    stbuf->st_atime = fileStat.st_atime; //horas de modificación etc ...
                    stbuf->st_mtime = fileStat.st_mtime;
                    stbuf->st_ctime = fileStat.st_ctime;
                    stbuf->st_size = fileStat.st_size; //tamaño
                    stbuf->st_blocks = stbuf->st_size / 512 + (stbuf->st_size % 512) ? 1 : 0; // tamaño divido entre 512

                }
                else
                    return -ENOENT;
            }

        }

    } else if (strncmp(path, "/Favoritos/",11) == 0) {


    }
    // Completar
    return 0;
}

int mp3_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                off_t offset, struct fuse_file_info *fi)
{   int i=0;
    struct estructura_mis_datos* datos = (struct estructura_mis_datos *)fuse_get_context()->private_data;


    if (strcmp(path, "/") == 0) {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "Artistas", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "Favoritos", NULL, 0)!=0) return -ENOMEM;

    } else if(strcmp(path, "/Artistas") == 0) {
        if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
        if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
        Artista aux=datos->lista_art;
        while(aux) {
            if(filler(buf, aux->tag_artista, NULL, 0)!=0) return -ENOMEM;
            aux=aux->sig;
        }

    } else if(strncmp(path, "/Artistas/",10) == 0) {

        char *token = strtok((char*)path, "/"); // busca primera parte del path
        token = strtok(NULL,"/");
        if(esta_artista(datos->lista_art,token)) // es un directorio de artista
        {
			char *artista=strdup(token);
            token = strtok(NULL,"/");
            if(token==NULL)
            {   // es el directorio
                if(filler(buf, ".", NULL, 0)!=0) return -ENOMEM;
                if(filler(buf, "..", NULL, 0)!=0) return -ENOMEM;
                DIR *dir;
                struct dirent *dp;
                char * file_name;
                dir = opendir(datos->path);
                while ((dp=readdir(dir)) != NULL) {
                    if ( !strcmp(dp->d_name, ".") || !strcmp(dp->d_name, "..") )
                    {
                        // do nothing (straight logic)
                    } else {
                        char lpath[1024]; // fichero en el path original
                        file_name = dp->d_name; // use it
                        strcpy(lpath,datos->path);
                        strcat(lpath,"/");
                        strcat(lpath,file_name);
                        fprintf(stderr,"  fichero: %s\n",lpath);

                        ID3v2_tag* artist_tag;
                        ID3v2_frame* artist_frame;
                        ID3v2_frame_text_content* artist_content;
                        artist_tag = load_tag(lpath);
                        if(artist_tag==NULL) continue;
                        artist_frame = tag_get_artist(artist_tag);

                        artist_content = parse_text_frame_content(artist_frame);

                        //    fprintf(stderr,"  Artist TAG: %s\n",artist_content->data);
                        limpia_etiqueta(artist_content->data);

                        if(strcmp(artist_content->data,artista)==0)   // tiene el artista
                            if(filler(buf, file_name, NULL, 0)!=0) return -ENOMEM;

                    }
                }
                closedir(dir);
                free(artista);
            }
            else return -ENOENT;
        }
        else return -ENOENT;
    }

    else if(strcmp(path, "/Favoritos") == 0) {
        if (filler(buf, ".", NULL, 0) != 0) return -ENOMEM;
        if (filler(buf, "..", NULL, 0) != 0) return -ENOMEM;


    } else
        return -ENOENT;

    return 0;
}


int mp3_open(const char* path, struct fuse_file_info* fi) {

    struct estructura_mis_datos* mis_datos = (struct estructura_mis_datos*)fuse_get_context()->private_data;
    
    char lpath[512];
    char * filename= basename((char *)path);
    strcpy(lpath,mis_datos->path);
    strcat(lpath,"/");
    strcat(lpath,filename);

    if (existe_fichero(filename, mis_datos->path)) // fichero
    {
        if ((fi->flags & 3) != O_RDONLY) return -EACCES;
        fi->fh=open(lpath,fi->flags);
        if(fi->fh<0) return -errno;
    }
    else
        return -ENOENT;

    return 0;
}


int mp3_read(const char* path, char* buf, size_t size, off_t offset, struct fuse_file_info* fi) {

    return pread(fi->fh, buf, size, offset);
}

int mp3_release(const char *path, struct fuse_file_info *fi)
{
    return close(fi->fh);
}

struct fuse_operations basic_oper = {
    .getattr = mp3_getattr,
    .readdir = mp3_readdir,
    .open = mp3_open,
    .read = mp3_read,
    .release = mp3_release

};


int main(int argc, char *argv[])
{
    struct estructura_mis_datos* mis_datos;
    mis_datos = malloc(sizeof(struct estructura_mis_datos));
    FILE *f;
    struct stat fileStat;

    // análisis parámetros de entrada
    if ((argc < 3) || (argv[argc - 2][0] == '-') || (argv[argc - 1][0] == '-'))
    {
        perror("Parametros insuficientes");
        exit(-1);
    }

    //  mis_datos->fichero_inicial = strdup(argv[argc - 2]); // fichero original
    mis_datos->path = realpath(argv[argc-2], NULL);
    if (mis_datos->path==NULL)
    {
        fprintf(stderr,"ERROR el directorio no existe\n");
        exit(-1);
    }
    argv[argc - 2] = argv[argc - 1];
    argv[argc - 1] = NULL;
    argc--;

    // leer metadatos del fichero
    fprintf(stderr,"real path: %s\n",mis_datos->path);

    stat(mis_datos->path, &fileStat);
    mis_datos->st_uid= fileStat.st_uid;
    mis_datos->st_gid= fileStat.st_gid;
    mis_datos->st_atime = fileStat.st_atime;
    mis_datos->st_ctime = fileStat.st_ctime;
    mis_datos->st_mtime = fileStat.st_mtime;
    fprintf(stderr,"Leyendo ficheros...\n");

    mis_datos->lista_art = rellenar_lista_artistas(mis_datos->path);
    fprintf(stderr,"ficheros indexados: %s\n",mis_datos->path);
    Artista aux=mis_datos->lista_art;
    while(aux) {
        fprintf(stderr," artista: %s\n",aux->tag_artista);
        aux=aux->sig;
    }
    return fuse_main(argc, argv, &basic_oper, mis_datos);
}

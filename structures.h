/*
 * FUSE: Filesystem in Userspace
 * DSO 2020
 * Prueba proyecto librería de música
*/

#ifndef __PRUEBA_MP3__
#define __PRUEBA_MP3__

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

  
// Esta es la estructura de un artista

struct lista_artista
{
    //char * path;						  
    //char * fichero_inicial;  				
    //char nombre_fichero;
    char *tag_artista;				// Nombre del artista
    //int numero_ficheros			 
    struct lista_artista *  sig;				// Puntero que apunta al artista siguiente en la lista

};

typedef struct lista_artista * Artista;

// Esta es la estructura de los ficheros mp3

struct estructura_mis_datos
{  				
	//int numero_ficheros;     				/* número de ficheros encontrados */
	char *path;								/* path donde se encuentran los ficheros mp3 */
	//char *mp3_inicial;						/* fichero mp3 inicial */ 
	//const char *nombre_ficheros[];			/* nombre de los ficheros */
	struct timespec st_atim;  				/* fechas del fichero */
    struct timespec st_mtim; 
    struct timespec st_ctim;  
    uid_t     st_uid;        				/* El usuario y grupo */
    gid_t     st_gid;  

    Artista lista_art;

    // Añadimos nuevos registros para controlar los TAGs del fichero mp3
   
	//~ char *titulo[];
	//~ char *artista[];
	//~ char *album[];
	//~ int year;
	//~ char *genero[];
	//~ int numero_pista;
	
};


#endif
